import java.util.Scanner;

public class q1 {
    static int per(int m ){
        if(m==1)
            return 0;
        if(m==2)
            return 1;
         
        return (m-1)*(per(m-1)+per(m-2));
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println(per(n));
    }
    
}

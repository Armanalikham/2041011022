import java.util.Scanner;

class path{
    double Xi , Yi , Xf , Yf;
    void set(){
        Scanner sc  = new Scanner(System.in);
        System.out.println("Enter X and Y coordinates of Initial position : ");
        Xi = sc.nextDouble();
        Yi = sc.nextDouble();
        System.err.println("Enter X and Y coordinates of Final position : ");
        Xf = sc.nextDouble();
        Yf = sc.nextDouble();
    }
    double cal(path p1){
        double dis = Math.sqrt(Math.pow(p1.Xf-p1.Xi, 2)+Math.pow(p1.Yf-p1.Yi, 2));
        return dis;
    }

}

public class q11 {
    public static void main(String[] args) {
        Scanner sc  = new Scanner(System.in);
        System.out.println("Enter the total no. of paths you want to compare for : ");
        int n = sc.nextInt();
        double brry[] = new double[n];
        path arry[] = new path [n];
        for(int i = 0 ; i<arry.length ; i++){
            System.out.println("Enter the constraints for "+(i+1)+" path:");
            arry[i] = new path();
            arry[i].set();
            brry[i]=arry[i].cal(arry[i]);
        }
        for(int i = 0 ; i < brry.length ;i++){
            System.out.println(brry[i]+" Units Distance will be covered in path "+(i+1));
        }
        int pos = 0;
        for(int i = 0 ; i < brry.length-1 ;i++){
            if(brry[i]<brry[i+1]){
                pos=(i+1);
            }
            else{
                pos=(i+2);
            }
        }
        System.out.println("You should go with path "+pos);

        
    }
}
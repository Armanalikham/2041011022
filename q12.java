import java.util.Scanner;

public class q12 {
    static char flip(char c){
        return (c == '0') ? '1' : '0';
    }
    static void comple2(String bin){
        int len = bin.length();
        int i;
        String c1 = "", c2 = "";
        for (i = 0; i < len; i++){
            c1 += flip(bin.charAt(i));
        }
        c2 = c1;
        for (i = len - 1; i >= 0; i--){
            if (c1.charAt(i) == '1'){
                c2 = c2.substring(0, i) + '0' + c2.substring(i + 1);
            }
            else{
                c2 = c2.substring(0, i) + '1' + c2.substring(i + 1);
                break;
            }
        }
        if (i == -1){
            c2 = '1' + c2;
        }
        System.out.println("2's complement: " + c2);
    }

    public static void main(String[] args) {
        Scanner sc  = new Scanner(System.in);
        int n = sc.nextInt();
        String bin =Integer.toBinaryString(n);
        comple2(bin);
        sc.close();
    }
}

import java.util.Scanner;

public class q13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the total no. of questions asked : ");
        int q  = sc.nextInt();
        String asker[] = new String[q];
        String solver[] = new String[q];
        for(int i = 0 ; i<q ;i++){
            System.out.println("Enter asker’s and coordinator’s name for "+(i+1)+" one:");
            asker[i] = sc.next();
            solver[i] = sc.next();
        }
        System.out.println("---------------------------------------------------------------------------------------");
        System.out.println("\t\tAsker\t\tQuery solved by\t");
        System.out.println("\t\t-------\t\t-----------------");
        for(int i = 0 ; i<q ;i++){
            System.out.println("\t\t"+asker[i]+"\t\t  "+solver[i]);
        }
        System.out.println("---------------------------------------------------------------------------------------");
    }
}
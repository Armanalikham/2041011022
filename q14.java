import java.util.Scanner;

public class q14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int j = 1 , p = 10 ;
        for(int i = 1 ; i <= n ; i++){
            if(i<11){
                System.out.print(i+", ");
            }
            else{
                if(j%2!=0){
                    p+=2;
                    System.out.print(p+", ");
                    j++;
                }
                else{
                    p+=9;
                    System.out.print(p+", ");
                    j++;
                }
            }
        }
    }
}
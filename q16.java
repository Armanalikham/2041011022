import java.util.Scanner;

public class q16 {
    static Scanner sc = new Scanner(System.in);
    static void prooving(int cox , int px , int coy , int py ,String eq){
        String derivative = (cox*px)+"x^"+(px-1)+"+"+(coy*py)+"y^"+(py-1);
        System.out.println("Derivative of the given equation : "+derivative);
        String integral = ((cox*px)/(px-1+1))+"x^"+(px-1+1)+"+"+((coy*py)/(py-1+1))+"y^"+(py-1+1);
        System.out.print("After integration we got "+integral);
        if(eq.equals(integral)){
            System.out.print(" , Hence proved .");
        }
        System.err.println();
        char c;
        do{
            System.out.print("Do you Want to solve the differential equation for any value of X and Y? ");
            c = sc.next().charAt(0);
            if(c=='y'){
                int x = sc.nextInt();
                int y = sc.nextInt();
                int sol = (int) ((cox)*Math.pow(x,px)+(coy)*Math.pow(y, py));
                System.out.println(" Value of the differential equation : "+sol);
            }
        }while(c == 'y');

    }
    public static void main(String[] args) {
        System.out.println("Enter the coefficient & power of x and y resp. ");
        int coeff_x = sc.nextInt();
        int powx = sc.nextInt();
        int coeff_y = sc.nextInt();
        int powy = sc.nextInt();
        String eq = coeff_x+"x^"+powx+"+"+coeff_y+"y^"+powy;
        System.out.println("Your equation : "+eq);
        prooving(coeff_x,powx,coeff_y,powy,eq);
    }
}

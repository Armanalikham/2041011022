public class q23 {
    static void altmerge(int krr[], int err[],
    int n1, int n2, int prr[])
{
         int i = 0, j = 0, k = 0;
         while (i < n1 && j < n2){         
           prr[k++] = krr[i++];
           prr[k++] = err[j++];
        }
        while (i < n1){
           prr[k++] = krr[i++];
        }
        while (j < n2){         
           prr[k++] = err[j++];
        }
}
    public static void main(String[] args) {
        int arr[] = {1,4,6,8};
        int brr[] = {11,19,10,7};
        int len = (arr.length + brr.length);
        int crr[] = new int[len];
        altmerge(arr, brr, arr.length, brr.length, crr);
        for(int i : crr){
            System.out.print(i+" ");
        }
    }
}
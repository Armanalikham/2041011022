import java.util.Scanner;

public class q28 {
    public static void main(String[] args) {
        Scanner sc  = new Scanner(System.in);
        int n  = sc.nextInt();
        int sum=0;
        for(int i = 0 ; i<n ; i++){
            int r = (int) Math.pow((i+1), (n-i));
            sum+=r;
            System.out.print(r+" ");
        }
        System.out.println("\nSum the the series = "+sum);
    }
    
}
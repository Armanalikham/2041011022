import java.util.Scanner;

public class q31 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int r = sc.nextInt();
        int c = sc.nextInt();
        int a[][] = new int [r][c];
        int sum = 0 ;
        System.out.println("Enter the Matrix elements :");
        for(int i = 0 ; i<r ;i++){
            for(int j = 0 ; j<c ; j++){
                a[i][j] = sc.nextInt();
            }
            System.out.println();
        }
        System.out.println("Matrix : ");
        for(int i = 0 ; i<r ; i++){
            for(int j = 0 ; j<c ; j++){
                System.out.print(a[i][j]+" ");
                if((i==0 || i==(r-1))||((j==0)||(j==(c-1)))){
                    sum+=a[i][j];
                }

            }
            
        }
        System.out.println("Sum of border elements of the matrix : "+sum);
    }
}
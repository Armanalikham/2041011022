import java.util.Scanner;

public class q36 {
    public static int catal(int n){
        int ele = 0;
        if (n <= 1){
            return 1;
        }
        for (int i = 0; i < n; i++){
            ele += catal(i)* catal(n - i - 1);
        }
        return ele; 
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter upto which term : ");
        int n = sc.nextInt();
        for(int i = 0 ; i <=n ; i++){
            System.out.print(catal(i)+" ");
        }
        sc.close();
        
    }
}

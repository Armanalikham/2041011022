class rectangle{
    int x;
    int y;
    rectangle(int x , int y){
        this.x = x;
        this.y = y;
    }
    
}
public class q4 {
    static Boolean isrec(rectangle r1 , rectangle r2 , rectangle r3 , rectangle r4){
        double comx,comy;
        double d1,d2,d3,d4;

        comx=(r1.x+r2.x+r3.x+r4.x)/4;
        comy=(r1.y+r2.y+r3.y+r4.y)/4;

        d1=Math.pow((comx-r1.x),2)+Math.pow((comy-r1.y),2);
        d2=Math.pow((comx-r2.x),2)+Math.pow((comy-r2.y),2);
        d3=Math.pow((comx-r3.x),2)+Math.pow((comy-r3.y),2);
        d4=Math.pow((comx-r4.x),2)+Math.pow((comy-r4.y),2);

        return (d1==d2 && d1==d3 && d1==d4);

    }
    public static void main(String[] args) {
        rectangle r1 = new rectangle(3, 4);
        rectangle r2 = new rectangle(4, 7);
        rectangle r3 = new rectangle(2, 0);
        rectangle r4 = new rectangle(1, 6);
        if(isrec(r1,r2,r3,r4)){
            System.out.println("Points form a rectangle");
        }
        else{
        System.out.println("Points does not form a rectangle");
        }
        

    }
    
}

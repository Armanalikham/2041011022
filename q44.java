import java.util.Scanner;

public class q44 {
    public static void main(String[] args) {
        Scanner sc  = new Scanner(System.in);
        int n = sc.nextInt();
        String s = Integer.toBinaryString(n);
        StringBuilder s1 = new StringBuilder(s);
        s1.reverse();
        if(s.equals(s1.toString())){
            System.out.println("Binary representation is palindromic");
        }
        else{
            System.out.println("Binary representation is not palindromic");
        }
        sc.close();
    }
}
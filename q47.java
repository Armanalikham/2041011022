import java.util.Scanner;

public class q47 {
    public static void main(String[] args) {
        Scanner sc  = new Scanner(System.in);
        int n  = sc.nextInt();
        int m = n ,sum=0;
        while(m>0){
            int rem = m%10;
            sum+=(rem*rem*rem);
            m/=10;
        }
        if (sum == n){
            System.out.println(n+" is a Armstrong number.");
        }
        else{
            System.out.println(n+" is not a  Armstrong number.");
        }
    }
}
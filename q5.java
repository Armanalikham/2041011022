class Point { 
    int x; int y; 
    public Point(int x, int y) { 
        super(); 
        this.x = x; 
        this.y = y; 
    } 
} 
class Rectangle { 
    private final Point tLeft; 
    private final Point bRight; 
    public Rectangle(Point tLeft, Point bRight) { 
        this.tLeft = tLeft; 
        this.bRight = bRight; 
    }
    public boolean isOverLapping(Rectangle samp) {
         if (this.tLeft.x > samp.bRight.x || this.bRight.x < samp.tLeft.x  || this.tLeft.y < samp.bRight.y || this.bRight.y > samp.tLeft.y) {
            return false;
        }
        else{
            return true;
        }
    }

public class q5 {
    public static void main(String[] args) { 
        Point l1 = new Point(1, 10);
        Point r1 = new Point(11, 22); 
        Point l2 = new Point(5, 8); 
        Point r2 = new Point(15, 33); 
        Rectangle first = new Rectangle(l1, r1); 
        Rectangle second = new Rectangle(l2, r2); 
        if (first.isOverLapping(second)) { 
            System.out .println("Yes, two rectangles are intersecting with each other");
         } 
            else { 
                System.out .println("No, two rectangles are not intersecting with each other"); 
        } 
    } 
} 
}

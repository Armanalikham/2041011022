  
import java.util.Scanner;
class complex{
    double real,img;
    void set(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter real and imaginary part of complex no : ");
        real = sc.nextDouble();
        img = sc.nextDouble();
    }
    void cal(complex a , complex b){
        real = a.real+b.real;
        img  = a.img+b.img;
    }
    void display(){
        System.out.println("Complex = "+real+" + i "+img);
    }
}
public class q55 {
    public static void main(String[] args) {
        complex c1 = new complex();
        c1.set();
        complex c2 = new complex();
        c2.set();
        complex c3 = new complex();
        c3.cal(c1,c2);
        c3.display();
        
    }
    
}
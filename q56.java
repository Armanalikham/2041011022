import java.util.Scanner;
public class q56 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = n , sum=0 , sum2 = 0;
        while(m>0){
            int rem  = m%10;
            sum = sum+rem;
            m/=10;
        }
        while(sum>0){
            int rem = sum%10;
            sum2 = sum2+rem;
            sum/=10;
        }
        if(sum2==1){
            System.out.println(n+" is a Magic number.");
        }
        else{
            System.out.println(n+" is not a Magic number.");
        }
    }
}
import java.util.Scanner;

public class q60 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a[][] = new int [3][3];
        int b[][] = new int [3][3];
        int c[][] = new int [3][3];
        //a matrix
        for(int i =0 ; i<3 ;i++){
            for(int j= 0 ; j<3 ; j++){
                a[i][j] = sc.nextInt();
            }
        }
        System.out.println();
        //b matrix
        for(int i =0 ; i<3 ;i++){
            for(int j= 0 ; j<3 ; j++){
                b[i][j] = sc.nextInt();
            }
        }
        // c matrix --> product of a & b matrix
        for(int i =0 ; i<3 ;i++){
            for(int j= 0 ; j<3 ; j++){
                c[i][j] = 0;
                for(int k = 0 ; k<3; k++){
                    c[i][j]+= a[i][k]*b[k][j];
                }
            }
        }
        System.out.println();
        for(int i =0 ; i<3 ;i++){
            for(int j= 0 ; j<3 ; j++){
               System.out.print(c[i][j]+" ");
            }
            System.out.println();
        }

        
    }
}

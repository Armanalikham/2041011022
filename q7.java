import java.util.Scanner;

public class q7 {
    public static void main(String[] args) {
        Scanner sc  = new Scanner(System.in);
        System.out.println("Enter the side of the square : ");
        int side = sc.nextInt();
        System.out.println("Area of circle inscribed in a square : "+((Math.PI/4)*side*side));
        sc.close();
    }   
}

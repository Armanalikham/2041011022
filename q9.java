import java.math.BigInteger;
import java.util.Scanner;

public class q9 {
    public static void main(String[] args) {
        Scanner sc  = new Scanner(System.in);
        BigInteger fact = BigInteger.ONE;
        long n = sc.nextInt();
        for(int i=1 ; i<=n ; i++){
            fact = fact.multiply(BigInteger.valueOf(i));

        }
        System.out.println(fact);
        sumOfdigits(fact.toString());
        sc.close();
    }
    static void sumOfdigits(String f){
        long sum = 0 ;
        for(int i = 0 ; i<f.length() ; i++){
            sum = sum + Long.parseLong(f.substring(i, (i+1)));
            System.out.print(f.charAt(i)+"|");
        }
        System.out.println("Sum = "+sum);
    }
}
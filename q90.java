import java.util.Scanner;

public class q90 {
    public static void main(String[] args) {
    Scanner sc  = new Scanner(System.in);
    int n = sc.nextInt();
    int m = n ,rev=0;
    while(m>0){
        int rem = m%10;
        rev = (rev*10)+rem;
        m/=10;
    }
    System.out.println("reverse of "+n+" is :"+rev);
    }
}
